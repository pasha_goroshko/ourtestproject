﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDelete.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Age { get; set; }
        public Sex Sex { get; set; }
        public DateTime Date { get; set; }
    }

    public enum Sex
    {
        none,
        female,
        male
    }
}

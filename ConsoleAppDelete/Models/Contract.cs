﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDelete.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public Client Client { get; set; }
        public Supplier Supplier { get; set; }
        public decimal Price { get; set; }
    }
}

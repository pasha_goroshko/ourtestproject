﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDelete.Models
{
    public class Supplier
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string SurName { get; set; }
    }
}

namespace ConsoleAppDelete.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Date", c => c.DateTime(nullable: false));

            var date = DateTime.Now.AddYears(-10).Date;

            Sql($"UPDATE dbo.Clients SET [Date] ='{date}'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "Date");
        }
    }
}

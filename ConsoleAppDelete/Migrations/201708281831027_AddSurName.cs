namespace ConsoleAppDelete.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSurName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "SurName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "SurName");
        }
    }
}

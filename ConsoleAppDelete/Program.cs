﻿using ConsoleAppDelete.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDelete
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new Context.Context())
            {
                var client = context.Clients.Where(x => x.Id == 2).FirstOrDefault();

                Contract contract = new Contract
                {
                    Price = 555,
                    Client = client
                };

                context.Clients.Add(client);

                context.SaveChanges();

                Console.WriteLine($"Client was save with ids {client.Id}");
            }

            Console.ReadLine();
        }
    }
}

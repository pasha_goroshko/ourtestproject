﻿using ConsoleAppDelete.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDelete.Context
{
    public class Context : DbContext
    {
        public Context() : base("ExampleContext")
        {
            
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
    }
}
